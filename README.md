WSD Project plan
================

### Group members ###
Name|Email address|Student ID
-----|-------------|----------
Tomáš Caha| tomas.caha@student.tut.fi| 270 726
Abhijit Das| abhijit.das@student.tut.fi| 256 332
Likai Ren| likai.ren@student.tut.fi| 267 122
Mohammad Imranur Rahman| rahman8@student.tut.fi| 267 636
Shaptarshi Basu| basu@student.tut.fi| 267 647

### Group name ###
__YounGo__

### Brief project description ###
In this project, we will develop an online game store where gamers can buy and play games, and game developers can sell their games.  
The complete description of the project is in [here](https://docs.google.com/document/d/1-ewnhsmQUpAdAhg6CiAEs34RXeRC5-dZ1wzEM0eZJ-A/edit)

### Features & implementations ###
* Players and developers login/registration: We will use the django authentication.
* Categories: We will help to users to find games by categorizing them. Every game will be added to one category. There will be also few special categories like most favourite, etc.
* Payment service: We use SIMPLE PAYMENT SERVICE, the mock payment service provided by the HEROKU.
* Browsing, adding, deleting, purchasing, saving and loading of game states, saving of game scores: We use the database to dynamically serve views to users and update data in the database whenever it will be neccessary.
* Security: We will pay attention to security (prevent players to play games they have not bought or developers to edit games they have not developed).
* Programming: We will use DRY principle and MVC/MVT separation

### Extra features ###
* Third-party login: We will try to implement at least one third-party login service (Facebook).
* Email validation: We will send email with an unique and random URL that must be clicked by the user to activate their account.
* Robot/Bot rejection: We will try to implement Google Recaptcha to prevent bots from registration.
* Mobile friendly: We will use Bootstrap to make the UI compatible for all screen sizes.
* Social media sharing: We will connect our store with Facebook through FB Like&Share buttons.

### Database/Model Structure ###
![picture alt](models_diagram.png "Database/Model Structure")

### Collaboration ###
We will meet weekly in the university. We have already created a private slack channel to communicate with each other. We are also maintaining a todo list in that channel.

### Running app locally ###
* Install python3 (if not already installed)
* Download project files in any local directory
* Open command line in that local directory path
* create a virtual environment
	* [Windows] `python -m venv env`
	* [Linux or OS X] `python3 -m venv env`
* Activate the virtual environment
	* [Windows] `env\Scripts\activate`
	* [Linux or OS X] `source env/bin/activate`
* Install dependencies:
	* `pip install -r requirements.txt`
* Run server:
	* `python manage.py runserver`
* From any internet browser go to __'http://localhost:8000'__

### Final report ###
Our project demonstration document can be found [here](https://docs.google.com/document/d/1MxRR10o1WH2E6Zeg2I6TgE9_EbobvF_J8fAdaO2iEps/edit?usp=sharing)
We divided tasks that 2 people will work on games and 2 on store because it is small project and it is not possible to include everybody to everything. Tomáš Caha and Abhijit Das were working on store. Likai Ren and Mohammad Imranur Rahman were working on javascript games. According to Mohammad Imranur Rahman new member, Shaptarshi Basu joined us in the middle after discussion with Mikko. He worked also on javascript game.

Each member of our team, who was involved in developing javascript games, they developed own games.
Second part of our team responsible for developing game store, both of them developed gamestore according to requirements.

### How to use ###
Go to https://wsd-gamestore.herokuapp.com/

You can login as a __player__ by __your Facebook account__ or __with these credentials__:
1. player1 12345
2. player2 12345
3. player3 12345

You can login as a __developer__ with these credentials:
1. admin gamestore1234
2. YounGo 1234
3. juurinen 1234
4. veli-matti 1234

Have fun!
