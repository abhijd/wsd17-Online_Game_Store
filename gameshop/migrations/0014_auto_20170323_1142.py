# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-03-23 11:42
from __future__ import unicode_literals

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gameshop', '0013_category_url_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='url_name',
            field=models.CharField(max_length=64, unique=True, validators=[django.core.validators.RegexValidator('^[a-z0-9_\\-]+$', message='Only small letters, digits and -_ are allowed')]),
        ),
    ]
