# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-03-20 15:23
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gameshop', '0010_profile_avatar'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='avatar',
            field=models.ImageField(default='avatars/none.png', upload_to='avatars/'),
        ),
    ]
