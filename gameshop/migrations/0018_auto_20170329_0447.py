# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-29 01:47
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('gameshop', '0017_auto_20170327_2048'),
    ]

    operations = [
        migrations.CreateModel(
            name='Payments',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('pid', models.CharField(max_length=255, unique=True)),
                ('amount', models.DecimalField(decimal_places=2, max_digits=7)),
                ('checksum', models.CharField(max_length=255, unique=True)),
                ('status', models.CharField(choices=[('0', 'pending'), ('1', 'success'), ('2', 'cancel'), ('3', 'error')], default='0', max_length=1)),
                ('buyer', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='buyer', to=settings.AUTH_USER_MODEL)),
                ('game', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='games', to='gameshop.Game')),
            ],
        ),
        migrations.RemoveField(
            model_name='purchasedgame',
            name='price',
        ),
        migrations.AlterField(
            model_name='purchasedgame',
            name='player',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='purchased_games', to=settings.AUTH_USER_MODEL),
        ),
    ]
