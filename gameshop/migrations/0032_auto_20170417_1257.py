# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-04-17 12:57
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('gameshop', '0031_auto_20170408_1746'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='email_verification_token',
            field=models.CharField(db_index=True, max_length=192, null=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='user',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='profile', to=settings.AUTH_USER_MODEL),
        ),
    ]
