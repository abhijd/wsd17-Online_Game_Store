from django.http import HttpResponseRedirect
from django.contrib import messages
from gameshop.models import Profile
from django.urls import reverse
from urllib.request import urlopen
from urllib.parse import urlparse
from django.core.files.base import File
from tempfile import NamedTemporaryFile
from hashlib import md5

# used for ensuring user has valid email
def require_email(strategy, backend, user, response, *args, **kwargs):
    if backend.name == 'facebook':
        if not response.get('email'):
            response = HttpResponseRedirect(reverse('login'))
            messages.error(strategy.request, 'You can not sign in because your profile does not contain valid email.')
            return response

# used for saving profile image
def save_facebook_profile(backend, user, response, *args, **kwargs):
    if backend.name == 'facebook':
        profile = user.profile
        if profile is None:
            profile = Profile(user=user)

        # always only player
        profile.role = Profile.PLAYER
        profile.save()

        # download profile photo & save it to db
        try:
            img_temp = NamedTemporaryFile(delete=True)
            user_unique_id = response.get('id')
            with urlopen('https://graph.facebook.com/' + user_unique_id + '/picture?width=300&height=300') as response:
                img_temp.write(response.read())
            img_temp.flush()
            filename = user_unique_id + '.jpg'

            # calculate checksums of avatars and if different update it
            try:
                with open(profile.avatar.path, 'rb') as current_avatar:
                    current_avatar_md5checksum = md5(current_avatar.read()).hexdigest()
            except:
                current_avatar_md5checksum = ''

            img_temp.seek(0)
            new_avatar_md5checksum = md5(img_temp.read()).hexdigest()

            if current_avatar_md5checksum != new_avatar_md5checksum:
                profile.avatar.save(filename, File(img_temp))

            img_temp.close()
            profile.save()
        except:
            return
