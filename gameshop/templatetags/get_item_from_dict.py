from django import template
register = template.Library()

@register.filter
def get_item_from_dict(d, key):
    return dict(d)[key]
