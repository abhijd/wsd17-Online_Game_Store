from django import template
import re
register = template.Library()

@register.filter
def search(value, search):
    return re.sub(search, '#2h=!CRDPtfH@Q8R&LSb3', value)

@register.filter
def replace(value, replace):
    return re.sub('#2h=!CRDPtfH@Q8R&LSb3', replace, value)
