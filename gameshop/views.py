from django.http import HttpResponse, Http404, HttpResponseNotFound, HttpResponseRedirect, HttpResponseForbidden, JsonResponse
from django.shortcuts import render, get_object_or_404
from django.template.loader import render_to_string
from django.core.mail import send_mail
from django.urls import reverse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.decorators import login_required
from django.db.models import Count, Case, When, Sum
from gameshop.models import Category, Game, Profile, Payment, PlayedGame
from gameshop.forms import SignUpForm, SignInForm, MyProfileForm, EditGameForm
from gameshop.functions import token_generator, render_to_string as rts, post_request
from django.conf import settings
from django.contrib import messages
from gameshop.decorators import anonymous_or_index, logged_or_denied, logged_or_login, developer_or_denied, player_or_denied
from django.core.files.uploadedfile import SimpleUploadedFile
from hashlib import md5
from json import loads as json_loads
from re import sub as re_sub

def home(request):
    return render(request, 'home.html', {'title': 'Main page', 'games': Game.objects.filter(deleted=None).order_by('?')[:4]})

def category(request, category_url_name='latest-games'):
    if category_url_name == 'latest-games':
        return render(request, 'category.html', {'title': 'Latest games', 'current_category_name': 'Latest games', 'current_category_url_name': 'latest-games', 'current_category_description': 'Here you can find the latest added games to our store.', 'categories': Category.objects.filter(deleted=None), 'games': Game.objects.filter(deleted=None).order_by('-published_at')[:18]})
    elif category_url_name == 'favourite-games':
        return render(request, 'category.html', {'title': 'Favourite games', 'current_category_name': 'Favourite games', 'current_category_url_name': 'favourite-games', 'current_category_description': 'Below you can see list of best-selling game of our store.', 'categories': Category.objects.filter(deleted=None), 'games': Game.objects.filter(deleted=None).annotate(purchased_games_count=Count(Case(When(payments__status=Payment.SUCCESS, then=1),))).filter(purchased_games_count__gt=0).order_by('-purchased_games_count')[:18] })
    elif category_url_name == 'my-games':
        if request.user.is_authenticated:
            if request.user.profile.role == Profile.DEVELOPER:
                current_category_description = 'Here you can see the list of games you have developed so far.'
            elif request.user.profile.role == Profile.PLAYER:
                current_category_description = 'Here you can see the list of games you have bought so far.'
            else:
                current_category_description = ''

            return render(request, 'category.html', {'title': 'My games', 'current_category_name': 'My games', 'current_category_url_name': 'my-games', 'current_category_description': current_category_description, 'categories': Category.objects.filter(deleted=None), 'games': request.user.profile.get_my_games_list})
        else:
            return HttpResponseForbidden()
    else:
        category = get_object_or_404(Category, url_name=category_url_name, deleted=None)
        return render(request, 'category.html', {'title': category.name, 'current_category_name': category.name, 'current_category_url_name': category.url_name, 'current_category_description': category.description, 'categories': Category.objects.filter(deleted=None), 'games': category.games.filter(deleted=None).order_by('?')})

@player_or_denied
def buy_game(request, game_url_name):
    game = get_object_or_404(Game, url_name=game_url_name, deleted=None)

    # check if user has already bought this game
    if game in request.user.profile.get_my_games_list:
        return HttpResponseRedirect(reverse('detail_game', kwargs={'game_url_name':game.url_name}))
    else:
        # prepare payment info
        pid = token_generator(96)
        amount = game.price
        checksumstr = 'pid={}&sid={}&amount={}&token={}'.format(pid, settings.SIMPLE_PAYMENTS_SELLER_ID, amount, settings.SIMPLE_PAYMENTS_SECRET_KEY)
        m = md5(checksumstr.encode('ascii'))
        checksum = m.hexdigest()

        # create payment entry with PENDING status
        payment = Payment(payment_id=pid, player=request.user.profile, game=game, price=game.price, checksum=checksum)
        payment.save()

        context = {
                   'title': 'Buy ' + game.name,
                   'game_name': game.name,
                   'simple_payments_url': settings.SIMPLE_PAYMENTS_URL,
                   'pid': pid,
                   'sid': settings.SIMPLE_PAYMENTS_SELLER_ID,
                   'amount': game.price,
                   'success_url': settings.SITE_URL + reverse('payment_confirm', kwargs={'payment_id': pid, 'checksum': checksum}),
                   'cancel_url': settings.SITE_URL + reverse('payment_cancel', kwargs={'payment_id': pid, 'checksum': checksum}),
                   'error_url': settings.SITE_URL + reverse('payment_error', kwargs={'payment_id': pid, 'checksum': checksum}),
                   'checksum': checksum
                  }
        return render(request, 'buy_game.html', context)

@player_or_denied
def payment_confirm(request, payment_id, checksum):
    payment = get_object_or_404(Payment, payment_id=payment_id)

    # check if user can access this payment
    if not payment in request.user.profile.get_my_payments_list:
        return HttpResponseForbidden()

    # payment info
    checksumstr = 'pid={}&ref={}&result={}&token={}'.format(request.GET['pid'], request.GET['ref'], request.GET['result'], settings.SIMPLE_PAYMENTS_SECRET_KEY)
    m = md5(checksumstr.encode('ascii'))
    checksum_check = m.hexdigest()

    # check if everything is OK
    if checksum_check == request.GET['checksum'] and payment.checksum == checksum and request.user == payment.player.user and not payment.status == Payment.SUCCESS:
        payment.status = Payment.SUCCESS
        payment.save()
        messages.success(request, 'Purchase is complete. You can play the game now.')
        return HttpResponseRedirect(reverse('detail_game', kwargs={'game_url_name': payment.game.url_name}))
    elif payment.status == Payment.SUCCESS:
        messages.info(request, 'You already purchased this game in the past. You can play the game now.')
        return HttpResponseRedirect(reverse('detail_game', kwargs={'game_url_name': payment.game.url_name}))
    else:
        messages.error(request, 'Purchase has not been successfully completed. Try it again or contact us.')
        return HttpResponseRedirect(reverse('index'))

@player_or_denied
def payment_cancel(request, payment_id, checksum):
    payment = get_object_or_404(Payment, payment_id=payment_id)

    # check if user can access this payment
    if not payment in request.user.profile.get_my_payments_list:
        return HttpResponseForbidden()

    # payment info
    checksumstr = 'pid={}&ref={}&result={}&token={}'.format(request.GET['pid'], request.GET['ref'], request.GET['result'], settings.SIMPLE_PAYMENTS_SECRET_KEY)
    m = md5(checksumstr.encode('ascii'))
    checksum_check = m.hexdigest()

    # check if everything is OK
    if checksum_check == request.GET['checksum'] and payment.checksum == checksum and request.user == payment.player.user and not payment.status == Payment.SUCCESS:
        payment.status = Payment.CANCEL
        payment.save()
        messages.warning(request, 'Purchase has been cancelled.')
    else:
        messages.error(request, 'Purchase has not been successfully completed. Try it again or contact us.')

    return HttpResponseRedirect(reverse('index'))

@player_or_denied
def payment_error(request, payment_id, checksum):
    payment = get_object_or_404(Payment, payment_id=payment_id)

    # check if user can access this payment
    if not payment in request.user.profile.get_my_payments_list:
        return HttpResponseForbidden()

    # payment info
    checksumstr = 'pid={}&ref={}&result={}&token={}'.format(request.GET['pid'], request.GET['ref'], request.GET['result'], settings.SIMPLE_PAYMENTS_SECRET_KEY)
    m = md5(checksumstr.encode('ascii'))
    checksum_check = m.hexdigest()

    # check if everything is OK
    if checksum_check == request.GET['checksum'] and payment.checksum == checksum and request.user == payment.player.user and not payment.status == Payment.SUCCESS:
        payment.status = Payment.ERROR
        payment.save()
        messages.error(request, 'An error has occurred during transaction.')
    else:
        messages.error(request, 'Purchase has not been successfully completed. Try it again or contact us.')

    return HttpResponseRedirect(reverse('index'))

#@logged_or_denied
#def payment_detail(request, payment_id):
#    payment = get_object_or_404(Payment, payment_id=payment_id)
#
#    # check if user can access this payment
#    if not payment in request.user.profile.get_my_payments_list:
#        return HttpResponseForbidden()
#
#    return render(request, 'detail_payment.html', {'title': 'Payment detail', 'payment': payment})

@logged_or_denied
def payments(request):
    context = {
               'title': 'List of your payments',
               'payments': request.user.profile.get_my_payments_list,
               'payments_status_choices': dict(Payment.PAYMENT_STATUS_CHOICES),
               'total_pending': request.user.profile.get_my_payments_list.filter(status=Payment.PENDING).aggregate(Sum('price'))['price__sum'],
               'total_success': request.user.profile.get_my_payments_list.filter(status=Payment.SUCCESS).aggregate(Sum('price'))['price__sum'],
               'total_cancel': request.user.profile.get_my_payments_list.filter(status=Payment.CANCEL).aggregate(Sum('price'))['price__sum'],
               'total_error': request.user.profile.get_my_payments_list.filter(status=Payment.ERROR).aggregate(Sum('price'))['price__sum'],
               'total': request.user.profile.get_my_payments_list.aggregate(Sum('price'))['price__sum']
               }

    return render(request, 'payments.html', context)

def detail_game(request, game_url_name):
    game = get_object_or_404(Game, url_name=game_url_name, deleted=None)
    return render(request, 'detail_game.html', {'title': game.name, 'categories': Category.objects.filter(deleted=None), 'game': game, 'site_url': settings.SITE_URL, 'facebook_app_id': settings.FACEBOOK_APP_ID })

@logged_or_login
def play_game(request, game_url_name):
    game = get_object_or_404(Game, url_name=game_url_name, deleted=None)

    # check if user can play this game (already bought or developer)
    if not game in request.user.profile.get_my_games_list:
        return HttpResponseForbidden()

    # game requests
    if request.method == 'POST':
        game_request = json_loads(request.body.decode('utf-8'))

        if game_request['messageType'] == 'SAVE':
            played_game, created = PlayedGame.objects.get_or_create(player=request.user.profile, game=game, status=PlayedGame.PAUSED)

            if game_request['gameState'] is None:
                played_game.state = ''
            else:
                played_game.state = str(game_request['gameState'])

            played_game.score = 0
            played_game.save()
            return JsonResponse({})
        elif game_request['messageType'] == 'SCORE':
            played_game, created = PlayedGame.objects.get_or_create(player=request.user.profile, game=game, status=PlayedGame.FINISHED)

            if game_request['score'] is None:
                played_game.score = 0
            else:
                played_game.score = game_request['score']

            played_game.save()
            return JsonResponse({})
        elif game_request['messageType'] == 'LOAD_REQUEST':
            try:
                played_game = PlayedGame.objects.get(player=request.user.profile, game=game, status=PlayedGame.PAUSED)
                return JsonResponse({'messageType': 'LOAD', 'gameState': json_loads(played_game.state.replace('\'', '"'))})
            except PlayedGame.DoesNotExist:
                return JsonResponse({'messageType': 'ERROR', 'info': 'Gamestate could not be loaded'})

    # now user can play this game (already bought or developer)
    return render(request, 'play_game.html', {'title': game.name, 'game': game, 'high_scores': game.played_games.filter(status=PlayedGame.FINISHED).order_by('-score')[:10]})

@developer_or_denied
def add_game(request):
    if request.method == 'POST':
        # create a form instance and populate it with data from the request
        form = EditGameForm(request.POST, request.FILES)

        if form.is_valid():
            # process the data in form.cleaned_data as required
            form_cleaned = form.clean()

            try:
                # create game
                game = Game(name=form_cleaned['name'],
                            url_name=re_sub('\w_\-', '', form_cleaned['name'].replace(' ','-')),
                            description=form_cleaned['description'],
                            url=form_cleaned['url'],
                            price=form_cleaned['price'],
                            image=form_cleaned['image'],
                            category=form_cleaned['category'],
                            developer=request.user.profile)
                game.save()
                messages.success(request, 'Your game has been published in our store.')
                return HttpResponseRedirect(reverse('detail_game', kwargs={'game_url_name': game.url_name}))
            except:
                messages.error(request, 'Something went wrong. Try it again or contact us.')
    else:
        form = EditGameForm()

    return render(request, 'edit_game.html', {'title': 'New game', 'form_title': 'New game', 'form': form, 'form_submit': 'Publish game'})

@developer_or_denied
def edit_game(request, game_url_name):
    game = get_object_or_404(Game, url_name=game_url_name, deleted=None)

    # check if user has already bought this game
    if not game in request.user.profile.get_my_games_list:
        return HttpResponseForbidden()

    if request.method == 'POST':
        # create a form instance and populate it with data from the request
        form = EditGameForm(request.POST, request.FILES)

        if form.is_valid():
            # process the data in form.cleaned_data as required
            form_cleaned = form.clean()

            try:
                # update game
                game.name = form_cleaned['name']
                game.url_name = re_sub('\w_\-', '', form_cleaned['name'].replace(' ','-'))
                game.description = form_cleaned['description']
                game.url = form_cleaned['url']
                game.price = form_cleaned['price']
                game.image = form_cleaned['image']
                game.category = form_cleaned['category']
                game.save()
                messages.success(request, 'Changes of your game have been successfully saved.')
                return HttpResponseRedirect(reverse('detail_game', kwargs={'game_url_name': game.url_name}))
            except:
                messages.error(request, 'Something went wrong. Try it again or contact us.')
    else:
        form = EditGameForm(instance=game)

    return render(request, 'edit_game.html', {'title': 'Edit game '+game.name, 'form_title': 'Edit game '+game.name, 'form': form, 'form_submit': 'Save changes'})

@developer_or_denied
def delete_game(request, game_url_name):
    game = get_object_or_404(Game, url_name=game_url_name, deleted=None)

    # check if developer can delete this game
    if not game in request.user.profile.get_my_games_list:
        return HttpResponseForbidden()

    if request.method == 'POST':
        game.is_deleted = True
        messages.success(request, 'Game has been successfully deleted.')
        return HttpResponseRedirect(reverse('index'))

    return render(request, 'delete_game.html', {'title': 'Delete game '+game.name, 'game_name': game.name})

@logged_or_login
def my_profile(request):
    social_profile = request.user.social_auth.count() > 0
    if request.method == 'POST':
        # create a form instance and populate it with data from the request
        form = MyProfileForm(request.POST, request.FILES, initial={'username': request.user.username, 'email': request.user.email, 'role': request.user.profile.role}, social_profile=social_profile)

        if form.is_valid():
            # process the data in form.cleaned_data as required
            form_cleaned = form.clean()

            # update fields with new information
            request.user.first_name = form_cleaned['first_name']
            request.user.last_name = form_cleaned['last_name']

            if form.cleaned_data['image']:
                request.user.profile.avatar = form.cleaned_data['image']

            request.user.save()

            if form.cleaned_data['password']:
                request.user.set_password(form.cleaned_data['password'])
                request.user.save()
                messages.warning(request, 'You have changed your password. Please sign in again.')
                return HttpResponseRedirect(reverse('index'))

            return HttpResponseRedirect(reverse('my_profile'))
    else:
        form = MyProfileForm(initial={'username': request.user.username, 'email': request.user.email, 'first_name': request.user.first_name, 'last_name': request.user.last_name, 'role': request.user.profile.role}, social_profile=social_profile)

    content_before_form = rts('<img src="{{user.profile.avatar.url}}" alt="avatar" class="avatar center-block" />', {'user': request.user})
    return render(request, 'my_profile.html', {'title': 'My profile', 'form_title': 'My profile', 'content_before_form': content_before_form, 'form': form, 'form_submit': 'Save changes', 'social_profile': social_profile})

def profile(request, username):
    user = get_object_or_404(User, username=username, profile__deleted=None)
    return render(request, 'user_profile.html', {'title': user.username + ' profile', 'profile': user.profile})

def high_scores(request):
    high_scores = PlayedGame.objects.filter(status=PlayedGame.FINISHED, game__deleted=None).order_by('-score')[:25]
    return render(request, 'scores.html', {'title': 'Global high scores', 'high_scores': high_scores})

@anonymous_or_index
def login(request):
    if request.method == 'POST':
        # create a form instance and populate it with data from the request
        form = SignInForm(request.POST)

        if form.is_valid():
            # process the data in form.cleaned_data as required
            form_cleaned = form.clean()

            # authenticate
            user = authenticate(username=form_cleaned['username'], password=form_cleaned['password'])

            # user and password is right
            if user is not None:
                if user.profile.is_verified and not user.profile.is_deleted:
                    if form_cleaned['remember_me']:
                        request.session.set_expiry(0) # when browser is closed
                    else:
                        request.session.set_expiry(86400) # 24 hrs

                    auth_login(request, user)

                    if form_cleaned['next']:
                        return HttpResponseRedirect(form_cleaned['next'])
                    else:
                        return HttpResponseRedirect(reverse('index'))
                else:
                    form.add_error(None, 'Your account needs to be activated. Check your email box for email with activation code.')
            else:
                logout(request)
                form.add_error(None, 'Your username and password didn\'t match. Please try again.')
    else:
        form = SignInForm()

    content_before_form = rts('<div class="form-group">Sign in as a Player via <a href="{% url "social:begin" "facebook" %}" class="btn btn-fb"><span class="fa fa-facebook"></span>&nbsp;&nbsp;Facebook</a> or</div>', {'user': request.user})
    content_after_form = rts('<div class="form-group"><a href="{% url "password_reset" %}">Forget the password?</a></div>', {})
    return render(request, 'registration/login.html', {'title': 'Sign in', 'form_title': 'Sign in', 'form': form, 'content_before_form': content_before_form, 'content_after_form': content_after_form, 'form_submit': 'Sign in'})

@anonymous_or_index
def register(request):
    if request.method == 'POST':
        # create a form instance and populate it with data from the request
        form = SignUpForm(request.POST)

        # check captcha
        captcha_value = request.POST['g-recaptcha-response']

        if not captcha_value:
            form.add_error(None, 'Please prove that you are not a robot by ticking the checkbox below!')
        else:
            recaptcha_validation = post_request(settings.GOOGLE_RECAPTCHA_URL, {'secret': settings.GOOGLE_RECAPTCHA_SECRETKEY, 'response': captcha_value})

            if recaptcha_validation['success'] and recaptcha_validation['hostname'] == settings.HOST_NAME:
                if form.is_valid():
                    # process the data in form.cleaned_data as required
                    form_cleaned = form.clean()

                    try:
                        # create user
                        user = User.objects.create_user(username=form_cleaned['username'], password=form_cleaned['password'], email=form_cleaned['email'], is_active=False, first_name=form_cleaned['first_name'], last_name=form_cleaned['last_name'])
                        user.profile.role = form_cleaned['role']
                        user.profile.email_verification_token = token_generator(192)
                        user.save()

                        # send email verification code
                        subject = 'YounGo Email Verification'
                        message = render_to_string('registration/verification_email.html', {'username': user.username, 'site_url': settings.SITE_URL, 'token': user.profile.email_verification_token})
                        from_email = settings.EMAIL_FROM
                        recipient_list = [user.email]
                        send_mail(subject, message, from_email, recipient_list, fail_silently=False)

                        messages.success(request, 'Your account has been successfully created and verification email has been sent to your email address.')
                    except:
                        messages.error(request, 'Something went terribly wrong. Try it again or contact us.')

                    return HttpResponseRedirect(reverse('index'))
            else:
                messages.error(request, 'Google reCAPTCHA error. Try it again or contact us.')
                print("Google reCAPTCHA error:")
                print(recaptcha_validation)
    else:
        form = SignUpForm()

    return render(request, 'registration/register.html', {'title': 'Sign up', 'form_title': 'Sign up', 'form': form, 'form_submit': 'Sign up', 'captcha': settings.GOOGLE_RECAPTCHA_SITEKEY })

@logged_or_denied
def logout(request):
    auth_logout(request)
    messages.success(request, 'You have been successfully signed out.')
    return HttpResponseRedirect(reverse('index'))

@anonymous_or_index
def verify_email(request, token):
    try:
        profile = Profile.objects.get(email_verification_token=token)
        profile.user.is_active = True
        profile.email_verification_token = None
        profile.user.save()

        messages.success(request, 'Your account has been successfully activated. You can sign in now.')
    except:
        messages.error(request, 'Verification token is invalid. User account has not been activated.')

    return HttpResponseRedirect(reverse('index'))

def about(request):
    return render(request, 'about.html', {})
