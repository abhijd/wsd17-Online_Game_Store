from django.http import HttpResponseRedirect, HttpResponseForbidden
from django.contrib import messages
from gameshop.models import Profile
from django.urls import reverse

# used for views where user cannot be logged-in, if  it is redirected to index
def anonymous_or_index(function=None):
    def _dec(view_func):
        def _view(request, *args, **kwargs):
            if request.user.is_authenticated():
                messages.error(request, 'Requested page cannot be accessed.')
                return HttpResponseRedirect('/')
            else:
                return view_func(request, *args, **kwargs)

        _view.__name__ = view_func.__name__
        _view.__dict__ = view_func.__dict__
        _view.__doc__ = view_func.__doc__

        return _view

    if function is None:
        return _dec
    else:
        return _dec(function)

# similar to login_required but it gives 403 instead
def logged_or_denied(function=None):
    def _dec(view_func):
        def _view(request, *args, **kwargs):
            if not request.user.is_authenticated():
                return HttpResponseForbidden()
            else:
                return view_func(request, *args, **kwargs)

        _view.__name__ = view_func.__name__
        _view.__dict__ = view_func.__dict__
        _view.__doc__ = view_func.__doc__

        return _view

    if function is None:
        return _dec
    else:
        return _dec(function)

# similar to login_required
def logged_or_login(function=None):
    def _dec(view_func):
        def _view(request, *args, **kwargs):
            if not request.user.is_authenticated():
                return HttpResponseRedirect(reverse('login'))
            else:
                return view_func(request, *args, **kwargs)

        _view.__name__ = view_func.__name__
        _view.__dict__ = view_func.__dict__
        _view.__doc__ = view_func.__doc__

        return _view

    if function is None:
        return _dec
    else:
        return _dec(function)

# simple decorator for checking if user is logged-in and if it is developer
def developer_or_denied(function=None):
    def _dec(view_func):
        def _view(request, *args, **kwargs):
            if not request.user.is_authenticated():
                return HttpResponseRedirect(reverse('login'))
            elif request.user.profile.role != Profile.DEVELOPER:
                return HttpResponseForbidden()
            else:
                return view_func(request, *args, **kwargs)

        _view.__name__ = view_func.__name__
        _view.__dict__ = view_func.__dict__
        _view.__doc__ = view_func.__doc__

        return _view

    if function is None:
        return _dec
    else:
        return _dec(function)

# simple decorator for checking if user is logged-in and if it is player
def player_or_denied(function=None):
    def _dec(view_func):
        def _view(request, *args, **kwargs):
            if not request.user.is_authenticated():
                return HttpResponseRedirect(reverse('login'))
            elif request.user.profile.role != Profile.PLAYER:
                return HttpResponseForbidden()
            else:
                return view_func(request, *args, **kwargs)

        _view.__name__ = view_func.__name__
        _view.__dict__ = view_func.__dict__
        _view.__doc__ = view_func.__doc__

        return _view

    if function is None:
        return _dec
    else:
        return _dec(function)
