from django.utils.translation import ugettext_lazy as _
import string
from random import choice as rand_token
from django.template import Template, Context

# for function post_request
from urllib.parse import urlencode
from urllib.request import Request as sendPost
from urllib.request import urlopen
from json import loads as str2json

def render_to_string(template, context):
    return Template(template).render(Context(context))

def token_generator(size=255, chars=string.ascii_letters + string.digits):
    return ''.join(rand_token(chars) for _ in range(size))

def post_request(url, post_fields):
    req = sendPost(url, urlencode(post_fields).encode())
    res = urlopen(req).read().decode()
    return str2json(res)
