from django.contrib import admin

from gameshop.models import *

# Register your models here.

admin.site.register(Profile)
admin.site.register(Game)
admin.site.register(Category)
admin.site.register(PlayedGame)
admin.site.register(Payment)
