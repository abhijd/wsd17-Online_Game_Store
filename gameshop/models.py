from django.core.urlresolvers import reverse
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth import get_user_model
from django.contrib.auth import models as auth_models
from django.db import models as models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.validators import RegexValidator, MinValueValidator
from decimal import Decimal
from django.utils import timezone

class SoftDeleteModel(models.Model):

    deleted = models.DateTimeField(null=True, default=None, blank=True)

    @property
    def is_deleted(self):
        return self.deleted is not None

    @is_deleted.setter
    def is_deleted(self, value):
        if value == True:
            self.deleted = timezone.now()
            self.save()
        elif value == False:
            self.deleted = None
            self.save()

    class Meta:
        abstract = True

class Profile(SoftDeleteModel):

    user = models.OneToOneField(auth_models.User, on_delete=models.CASCADE, related_name='profile', null=True)
    avatar = models.ImageField(upload_to='avatars/', default='avatars/none.png')
    email_verification_token = models.CharField(max_length=192, null=True, db_index=True)

    PLAYER = '1'
    DEVELOPER = '2'
    USER_ROLE_CHOICES = (
        (PLAYER, 'Player'),
        (DEVELOPER, 'Developer')
    )
    role = models.CharField(max_length=1, choices=USER_ROLE_CHOICES, default=PLAYER)

    @property
    def is_verified(self):
        # If email_verification_token is empty, user is verified, there is no need to store email verification token
        if self.email_verification_token:
            return False
        else:
            return True

    @property
    def get_role_name(self):
        roles = dict(self.USER_ROLE_CHOICES)
        return roles[self.role]

    @property
    def get_my_games_list(self):
        if self.role == self.DEVELOPER:
            return self.developed_games.filter(deleted=None).order_by('name')
        elif self.role == self.PLAYER:
            # is it possible to do it better? I do not know Django ORM and I am kinda lost in Django ORM
            purchased_games = self.payments.filter(status=Payment.SUCCESS)
            game_ids = []

            for purchased_game in purchased_games:
                game_ids.append(purchased_game.game.id)

            return Game.objects.filter(id__in=game_ids, deleted=None).order_by('name')
        else:
            return None

    @property
    def get_my_payments_list(self):
        if self.role == self.DEVELOPER:
            return Payment.objects.filter(game__in=self.developed_games.all()).order_by('-updated_at')
        elif self.role == self.PLAYER:
            return self.payments.all().order_by('-updated_at')
        else:
            return None

    def __str__(self):
        return ' '.join([
            self.user.username,
            '(' + self.user.email + ')',
        ])

    def set_user_delete(self):
        # return get_user_model().objects.get_or_create(is_active = False)[0]
        pass

    @receiver(post_save, sender=auth_models.User)
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance)

    @receiver(post_save, sender=auth_models.User)
    def save_user_profile(sender, instance, **kwargs):
        instance.profile.save()

class Game(SoftDeleteModel):

    name = models.CharField(max_length=255, validators=[RegexValidator('^[\w_\- ]+$')])
    url_name = models.CharField(max_length=512, unique=True, validators=[RegexValidator('^[\w_\-]+$')])
    description = models.TextField(max_length=1024)
    url = models.URLField()
    price = models.DecimalField(max_digits=7, decimal_places=2, validators=[MinValueValidator(Decimal('0.01'))])
    image = models.ImageField(upload_to='games/', default='games/none.png')
    developer = models.ForeignKey('gameshop.Profile', on_delete=models.CASCADE, related_name='developed_games', null=True)
    published_at = models.DateTimeField(auto_now_add=True, editable=False)
    category = models.ForeignKey('gameshop.Category', on_delete=models.SET_NULL, related_name='games', null=True)

    def __str__(self):
        return self.name

class Category(SoftDeleteModel):

    name = models.CharField(max_length=64)
    url_name = models.CharField(max_length=64, unique=True, validators=[
        RegexValidator('^[a-z0-9_\-]+$',
            message='Only small letters, digits and -_ are allowed'
        ),
    ])
    description = models.TextField(max_length=1024)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Categories"

class PlayedGame(models.Model):

    player = models.ForeignKey('gameshop.Profile', on_delete=models.CASCADE, related_name='played_games')
    game = models.ForeignKey('gameshop.Game', on_delete=models.CASCADE, related_name='played_games')
    score = models.DecimalField(max_digits=8, decimal_places=1, default=0, validators=[MinValueValidator(Decimal('0'))])
    updated_at = models.DateTimeField(auto_now=True, editable=False)
    state = models.CharField(max_length=1024, null=True)

    PAUSED = '1'
    FINISHED = '2'
    PLAYED_GAME_STATE_CHOICES = (
        (PAUSED, 'Paused'),
        (FINISHED, 'Finished')
    )
    status = models.CharField(max_length=1, choices=PLAYED_GAME_STATE_CHOICES, default=FINISHED)

    def __str__(self):
        return ' '.join([
            self.game.name,
            '[' + str(self.player) + '] =',
            str(self.score),
            '(' + dict(self.PLAYED_GAME_STATE_CHOICES)[self.status] + ')',
        ])

class Payment(models.Model):

    payment_id = models.CharField(max_length=96, primary_key=True)
    game = models.ForeignKey('gameshop.Game', on_delete=models.SET_NULL, related_name='payments', null=True)
    player = models.ForeignKey('gameshop.Profile', on_delete=models.SET_NULL, related_name='payments', null=True)
    price = models.DecimalField(max_digits=7, decimal_places=2)
    checksum = models.CharField(max_length=32, unique=True)
    updated_at = models.DateTimeField(auto_now_add=True, editable=False)

    PENDING = '0'
    SUCCESS = '1'
    CANCEL = '2'
    ERROR = '3'
    PAYMENT_STATUS_CHOICES = (
        (PENDING, 'Pending'),
        (SUCCESS, 'Success'),
        (CANCEL, 'Cancel'),
        (ERROR, 'Error'),
    )
    status = models.CharField(max_length=1, choices=PAYMENT_STATUS_CHOICES, default=PENDING)

    def __str__(self):
        return ' '.join([
            self.game.name,
            '[' + str(self.player) + '] =',
            str(self.price),
            ' (' + dict(self.PAYMENT_STATUS_CHOICES)[self.status] + ')',
        ])
