from django import forms
from django.core.exceptions import ValidationError
from gameshop.models import Profile, Game, Category
from django.contrib.auth.models import User

class SignUpForm(forms.Form):

    username = forms.RegexField(max_length=150, label='Username', regex=r'^[\w.@+-]+$', error_messages={'invalid': 'This value may contain only letters, numbers and @/./+/-/_/#/! characters.'})
    password = forms.CharField(max_length=255, min_length=5, label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(max_length=255, min_length=5, label='Password again', widget=forms.PasswordInput)
    email = forms.EmailField(max_length=255, label='Email')
    first_name = forms.RegexField(max_length=30, label='First Name', regex=r'[^\s\d.@+-\\\/\<\>]+$', error_messages={'invalid': 'This value may contain only letters.'})
    last_name = forms.RegexField(max_length=30, label='Last Name', regex=r'[^\s\d.@+-\\\/\<\>]+$', error_messages={'invalid': 'This value may contain only letters.'})

    role = forms.ChoiceField(
        widget = forms.Select(),
        choices = Profile.USER_ROLE_CHOICES,
        required = True,
        label = 'I am a'
    )

    def clean(self):
        # check for username
        if User.objects.filter(username=self.cleaned_data.get('username')).exists():
            raise forms.ValidationError('Username is taken by someone else')

        # check for email
        if User.objects.filter(email=self.cleaned_data.get('email')).exists():
            raise forms.ValidationError('Email is used by someone else')

        # check passwords
        if self.cleaned_data.get('password') != self.cleaned_data.get('password2'):
            raise forms.ValidationError(('Password doesn\'t match'),code='invalid')

        return self.cleaned_data

class SignInForm(forms.Form):

    username = forms.CharField(max_length=30, label='Username')
    password = forms.CharField(max_length=255, label='Password', widget=forms.PasswordInput)
    remember_me = forms.BooleanField(required=False, label='Remember me')
    next = forms.CharField(widget=forms.HiddenInput(), required=False)

    def clean(self):
        return self.cleaned_data

class MyProfileForm(forms.Form):

    username = forms.CharField(max_length=150, label='Username', disabled=True, required=False)
    password = forms.CharField(max_length=255, min_length=5, label='Password', widget=forms.PasswordInput, required=False)
    password2 = forms.CharField(max_length=255, min_length=5, label='Password again', widget=forms.PasswordInput, required=False)
    email = forms.EmailField(max_length=255, label='Email', disabled=True, required=False)
    first_name = forms.RegexField(max_length=30, label='First Name', regex=r'[^\s\d.@+-\\\/\<\>]+$', error_messages={'invalid': 'This value may contain only letters.'})
    last_name = forms.RegexField(max_length=30, label='Last Name', regex=r'[^\s\d.@+-\\\/\<\>]+$', error_messages={'invalid': 'This value may contain only letters.'})
    role = forms.ChoiceField(
        widget = forms.Select(),
        choices = Profile.USER_ROLE_CHOICES,
        label = 'I am a',
        required = False,
        disabled = True
    )
    image = forms.ImageField(required=False)

    def __init__(self, *args, **kwargs):
        social_profile = kwargs.get('social_profile', False)
        if 'social_profile' in kwargs:
            del kwargs['social_profile']
        super(MyProfileForm, self).__init__(*args, **kwargs)
        if social_profile:
            del self.fields['password']
            del self.fields['password2']
            del self.fields['image']
            self.fields['first_name'].disabled = True
            self.fields['last_name'].disabled = True

    def clean(self):
        # check passwords
        if self.cleaned_data.get('password') or self.cleaned_data.get('password2'):
            if self.cleaned_data.get('password') != self.cleaned_data.get('password2'):
                raise forms.ValidationError('Password doesn\'t match')

        return self.cleaned_data

class EditGameForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(EditGameForm, self).__init__(*args, **kwargs)
        self.fields['category'].queryset = Category.objects.filter(deleted=None)

    class Meta:
        model = Game
        fields = ['name', 'description', 'url' , 'price', 'image', 'category']

    def clean(self):
        return self.cleaned_data
