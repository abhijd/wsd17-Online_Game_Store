from django.conf.urls import url, include
from django.contrib.auth import views as auth_views
from gameshop import views
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import TemplateView

urlpatterns = [
    url(r'^$', views.home, name='index'),
    url(r'^category/$', views.category, {'category_name': 'latest-games'}),
    url(r'^category/(?P<category_url_name>[a-z0-9_\-]+)/$', views.category, name='category'),
    url(r'^game/add/$', views.add_game, name='add_game'),
    url(r'^game/(?P<game_url_name>[\w_\-]+)/buy/$', views.buy_game, name='buy_game'),
    url(r'^game/(?P<game_url_name>[\w_\-]+)/detail/$', views.detail_game, name='detail_game'),
    url(r'^game/(?P<game_url_name>[\w_\-]+)/play', views.play_game, name='play_game'),
    url(r'^game/(?P<game_url_name>[\w_\-]+)/edit/$', views.edit_game, name='edit_game'),
    url(r'^game/(?P<game_url_name>[\w_\-]+)/delete/$', views.delete_game, name='delete_game'),
    url(r'^payments/$', views.payments, name='payments'),
    #url(r'^payment/(?P<payment_id>[\w]{96})/detail/$', views.payment_detail, name='payment_detail'),
    url(r'^payment/(?P<payment_id>[\w]{96})/confirm/(?P<checksum>[\w]{32})/$', views.payment_confirm, name='payment_confirm'),
    url(r'^payment/(?P<payment_id>[\w]{96})/cancel/(?P<checksum>[\w]{32})/$', views.payment_cancel, name='payment_cancel'),
    url(r'^payment/(?P<payment_id>[\w]{96})/error/(?P<checksum>[\w]{32})/$', views.payment_error, name='payment_error'),
    url(r'^highscores/$', views.high_scores, name='high_scores'),
    url(r'^about/$', views.about, name='about'),
    url(r'^login/$', views.login, name='login'),
    url(r'^logout/$', views.logout, name='logout'),
    url(r'^oauth/', include('social_django.urls', namespace='social')),
    url(r'^user/register/$', views.register, name='register'),
    url(r'^user/verify-email/(?P<token>[\w]{192})/$', views.verify_email, name='verify_email'),
    url(r'^user/profile/$', views.my_profile, name='my_profile'),
    url(r'^user/profile/(?P<username>[\w.@+-]+)/$', views.profile, name='user_profile'),
    url(r'^user/password/reset/$', auth_views.password_reset,
    {
    'template_name': 'registration/password_reset_form.html',
    'email_template_name': 'registration/password_reset_email.txt',
    'html_email_template_name': None,
    'subject_template_name': 'registration/password_reset_email_subject.txt',
    'post_reset_redirect' : '/user/password/reset/done/'
    },
    name='password_reset'),
    url(r'^user/password/reset/done/$', auth_views.password_reset_done),
    url(r'^user/password/reset/(?P<uidb64>[\w_\-]+)/(?P<token>[\w]{1,13}-[\w]{1,20})/$', auth_views.password_reset_confirm, {'post_reset_redirect': '/user/password/done/'}, name='password_reset_confirm'),
    url(r'^user/password/done/$', auth_views.password_reset_complete),
    url(r'^example-game/$', TemplateView.as_view(template_name='example-game/example_game.html', content_type='text/html'), name='example-game')
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
