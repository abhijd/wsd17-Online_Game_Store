$(document).ready(function()
{
  'use strict';
  const game_url = $('#game-iframe').attr('src').toLowerCase();

  // catch messages from game
  $(window).on('message', function(e)
  {
    if (e.originalEvent.origin.toLowerCase() == game_url || e.originalEvent.origin.toLowerCase() + '/' == game_url || game_url.indexOf(e.originalEvent.origin.toLowerCase()) == 0)
    {
      var data = e.originalEvent.data;

      // messages
      switch(data.messageType)
      {
        case 'SETTING':
          // width of game
          if (data.options.width > 0 )
          {
            var game_width = $('#game-iframe').width();

            // change width only if current game width is smaller than needed
            if (game_width < data.options.width)
            $('#game-iframe').width(data.options.width + 'px');
          }

          // height of game
          if (data.options.height > 0)
          $('#game-iframe').height(data.options.height + 'px');
          break;

        case 'SCORE':
          pushMessageToStore(data, null, modalMessage('Score has been submitted'));
          break;

        case 'SAVE':
          pushMessageToStore(data, null, modalMessage('Game has been saved'));
          break;

        case 'LOAD_REQUEST':
          pushMessageToStore(data, loadGame);
          break;
      }
    }
    else
    {
      alert('Origin of the game is different from origin recorded in store');
    }
  });

  function pushMessageToStore(data, callback, modal_callback)
  {
    $.ajax({
      url: window.location.href,
      type: 'POST',
      data: JSON.stringify(data),
      cache: false,
      dataType: 'json',
      processData: false,
      contentType: 'text/plain',
      beforeSend: function(xhr, settings)
      {
        if (settings.type == 'POST' && !this.crossDomain)
        xhr.setRequestHeader('X-CSRFToken', getCookie('csrftoken'));
      },
    }).done(function(data)
    {
      // for testing
      console.log('Data has been successfully pushed to the store. Response:');
      console.log(data);

      // call callbacks
      typeof callback === 'function' && callback(data);
      typeof modal_callback === 'function' && modal_callback();
    }).fail(function()
    {
      alert('Sorry. Server unavailable.');
    });
  };

  function getCookie(name)
  {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '')
    {
      var cookies = document.cookie.split(';');
      for (var i = 0; i < cookies.length; i++)
      {
        var cookie = jQuery.trim(cookies[i]);
        // Does this cookie string begin with the name we want?
        if (cookie.substring(0, name.length + 1) === (name + '='))
        {
          cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
          break;
        }
      }
    }
    return cookieValue;
  }

  function loadGame(data)
  {
    if (data.messageType == 'ERROR')
    {
      modalMessage('No saved game to load');
    }
    else
    {
      $('#game-iframe')[0].contentWindow.postMessage(data, game_url);
    }
  }

  function modalMessage(msg)
  {
    $('.modal-title').text(msg);
    $('#resultModal').modal('toggle');
  }
});
