from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views as auth_views
from gameshop import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include('gameshop.urls') ),

]
